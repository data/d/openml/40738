# OpenML dataset: data-v3.en-es-lit.clean.anno_without_14top.uniform

https://www.openml.org/d/40738

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The English Spanish mappings (literals), annotated (binary), without the 14 upper (top) rows. This dataset is used to model a classificator. The classificator will be validated against the 14 upper rows (not shown by the model).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40738) of an [OpenML dataset](https://www.openml.org/d/40738). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40738/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40738/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40738/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

